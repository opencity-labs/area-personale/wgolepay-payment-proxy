FROM tiangolo/uvicorn-gunicorn-fastapi:python3.10

#set timezone
ENV TZ=Europe/Rome

RUN mkdir -p payments_proxy/tests/log

COPY . /code

# Set the working directory
WORKDIR /code/payments_proxy

# create dir for configuration
RUN mkdir -p /code/payments_proxy/configuration/data
RUN mkdir -p /code/payments_proxy/configuration/data/tenants
RUN mkdir -p /code/payments_proxy/configuration/data/events

# Copy requirements.txt and install dependencies
COPY ./payments_proxy/requirements.txt /code/payments_proxy/requirements.txt
RUN pip install --no-cache-dir -r /code/payments_proxy/requirements.txt

# Expose the debugging port
EXPOSE 5678
EXPOSE 8000

# Start the application with uvicorn and enable ptvsd for debugging
CMD ["python", "-m", "ptvsd", "--host", "0.0.0.0", "--port", "5678", "/usr/local/bin/uvicorn", "main_api:app", "--host", "0.0.0.0", "--port", "8000"]
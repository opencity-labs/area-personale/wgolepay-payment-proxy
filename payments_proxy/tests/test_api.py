import json
import requests
import uuid

BASE_URL = "http://localhost:8000"

# generate random ids
new_tenant_to_create = str(uuid.uuid4())
tenant_to_not_find = str(uuid.uuid4())
new_config_to_create = str(uuid.uuid4())
config_to_not_find = str(uuid.uuid4())
payment_to_not_find = str(uuid.uuid4())
valid_payment_id = str(uuid.uuid4())

with open("tests/configuration_config.json", "r") as file:
    config_payload = json.load(file)

with open("tests/configuration_tenant.json", "r") as file:
    tenant_payload = json.load(file)


# Test GET Endpoints
def test_get_metrics():
    response = requests.get(f"{BASE_URL}/metrics")
    assert response.status_code == 200


def test_get_status():
    response = requests.get(f"{BASE_URL}/status")
    assert response.status_code == 200


# Test Payment and Notice Endpoints
# def test_get_online_payment():
#     response = requests.get(f"{BASE_URL}/online-payment/{valid_payment_id}")
#     assert response.status_code == 200

# def test_get_notice():
#     response = requests.get(f"{BASE_URL}/notice/{valid_payment_id}")
#     assert response.status_code == 200

# def test_get_receipt():
#     response = requests.get(f"{BASE_URL}/receipt/{valid_payment_id}")
#     assert response.status_code == 200

# def test_get_update():
#     response = requests.get(f"{BASE_URL}/update/{valid_payment_id}")
#     assert response.status_code == 200

# def test_get_landing():
#     response = requests.get(f"{BASE_URL}/landing/{valid_payment_id}")
#     assert response.status_code == 200


def test_get_not_existing_online_payment():
    response = requests.get(f"{BASE_URL}/online-payment/{payment_to_not_find}")
    assert response.status_code == 404


def test_get_not_existing_notice():
    response = requests.get(f"{BASE_URL}/notice/{payment_to_not_find}")
    assert response.status_code == 404


def test_get_not_existing_receipt():
    response = requests.get(f"{BASE_URL}/receipt/{payment_to_not_find}")
    assert response.status_code == 404


def test_get_not_existing_update():
    response = requests.get(f"{BASE_URL}/update/{payment_to_not_find}")
    assert response.status_code == 404


def test_get_not_existing_landing():
    response = requests.get(f"{BASE_URL}/landing/{payment_to_not_find}")
    assert response.status_code == 404


# Test POST for creating tenants
def test_create_not_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.post(f"{BASE_URL}/tenants/", json=tenant_payload)
    assert response.status_code == 201


def test_create_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.post(f"{BASE_URL}/tenants/", json=tenant_payload)
    assert response.status_code == 409


# Test GET for retrieving non-existing tenant
def test_get_not_existing_tenant():
    response = requests.get(f"{BASE_URL}/tenants/{tenant_to_not_find}")
    assert response.status_code == 404


# Test PUT for updating tenants
def test_update_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.put(
        f"{BASE_URL}/tenants/{new_tenant_to_create}", json=tenant_payload
    )
    assert response.status_code == 201


def test_update_not_existing_tenant():
    tenant_payload["id"] = tenant_to_not_find
    response = requests.put(
        f"{BASE_URL}/tenants/{tenant_to_not_find}", json=tenant_payload
    )
    assert response.status_code == 404


# Test PATCH for updating tenants
def test_patch_existing_tenant():
    tenant_payload["id"] = new_tenant_to_create
    response = requests.patch(
        f"{BASE_URL}/tenants/{new_tenant_to_create}", json=tenant_payload
    )
    assert response.status_code == 201


def test_patch_not_existing_tenant():
    tenant_payload["id"] = tenant_to_not_find
    response = requests.patch(
        f"{BASE_URL}/tenants/{tenant_to_not_find}", json=tenant_payload
    )
    assert response.status_code == 404


# Test DELETE for deleting tenants
def test_delete_not_existing_tenant():
    response = requests.delete(f"{BASE_URL}/tenants/{tenant_to_not_find}")
    assert response.status_code == 404


def test_delete_existing_tenant():
    response = requests.delete(f"{BASE_URL}/tenants/{new_tenant_to_create}")
    assert response.status_code == 204


# Test POST for creating configs
def test_create_not_existing_config():
    config_payload["id"] = new_config_to_create
    response = requests.post(f"{BASE_URL}/configs/", json=config_payload)
    assert response.status_code == 201


def test_create_existing_config():
    config_payload["id"] = new_config_to_create
    response = requests.post(f"{BASE_URL}/configs/", json=config_payload)
    assert response.status_code == 409


# Test GET for retrieving non-existing config
def test_get_not_existing_config():
    response = requests.get(f"{BASE_URL}/configs/{config_to_not_find}")
    assert response.status_code == 404


# Test GET for retrieving existing config
def test_get_existing_config():
    response = requests.get(f"{BASE_URL}/configs/{new_config_to_create}")
    assert response.status_code == 200


# Test PUT for updating configs
def test_update_existing_config():
    config_payload["id"] = new_config_to_create
    response = requests.put(
        f"{BASE_URL}/configs/{new_config_to_create}", json=config_payload
    )
    assert response.status_code == 201


def test_update_not_existing_config():
    config_payload["id"] = config_to_not_find
    response = requests.put(
        f"{BASE_URL}/configs/{config_to_not_find}", json=config_payload
    )
    assert response.status_code == 404


# Test PATCH for updating configs
def test_patch_existing_config():
    config_payload["id"] = new_config_to_create
    response = requests.patch(
        f"{BASE_URL}/configs/{new_config_to_create}", json=config_payload
    )
    assert response.status_code == 201


def test_patch_not_existing_config():
    config_payload["id"] = config_to_not_find
    response = requests.patch(
        f"{BASE_URL}/configs/{config_to_not_find}", json=config_payload
    )
    assert response.status_code == 404


# Test DELETE for deleting configs
def test_delete_not_existing_config():
    response = requests.delete(f"{BASE_URL}/configs/{config_to_not_find}")
    assert response.status_code == 404


def test_delete_existing_config():
    response = requests.delete(f"{BASE_URL}/configs/{new_config_to_create}")
    assert response.status_code == 204

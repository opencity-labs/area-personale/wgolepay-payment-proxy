from __future__ import annotations
import json
import requests
from models.logger import get_logger
from models.types import RestHeaderClient
from requests.auth import HTTPBasicAuth
from models.excepitions import WGolEPayException
from settings import PAGOPA_CHECKOUT_URL, DEBUG


log = get_logger()


class RestClient(object):
    def __init__(self):
        self.base_url = ""
        self.username = ""
        self.pwd = ""
        self.pago_pa_url = PAGOPA_CHECKOUT_URL

    def _set_auth_token(self, configuration: dict):
        self.base_url = configuration["payments_api_url"]
        self.username = configuration["username"]
        self.pwd = configuration["password"]

    def _get_auth_token(self, configuration: dict):
        if not self.username or not self.pwd:
            self._set_auth_token(configuration)
        return HTTPBasicAuth(self.username, self.pwd)

    def _check_errors(self, response):
        if (
            response["descrizioneerrore"] or response["codiceerrore"]
        ):  # modifcare in base al codice di errore
            raise WGolEPayException(
                f'Codice: {response["codiceerrore"]} | '
                f'Descrizione: {response["descrizioneerrore"]}',
            )

    def create_debtor_position_and_return_payment_url(
        self, data: dict, configuration: dict
    ):
        """
        Servizio di pagamento presso il portale dell’ente.
        Questo servizio consente il pagamento di più posizioni debitorie fino ad un massimo di 5 attraverso
        l’utilizzo di un carrello di posizioni debitorie.
        Questo servizio si utilizza nell’ambito di implementazione di un portale web di pagamento.
        Il servizio fornirà una Responce con l’esito dell’operazione.
        Args:
            data (dict): Dati della posizione debitore
        """
        try:
            auth = self._get_auth_token(configuration)
            url = f"{self.base_url}/uploadcarrellopd"
            headers = RestHeaderClient.APPLICATION_JSON.value
            response = requests.post(url, headers=headers, auth=auth, json=data)
            if response.status_code == 200:
                response_data = response.json()
                self._check_errors(response_data)
                return response_data
            else:
                log.error(
                    f"Errore nella richiesta. Codice di stato: {response.status_code}",
                    exc_info=DEBUG,
                )
                self._check_errors(response_data)
                raise WGolEPayException(response.text)
        except Exception as e:
            log.error(f"Errore durante la richiesta: {e}", exc_info=DEBUG)
            return None

    def download_paper_payment_document(self, data: dict, configuration: dict):
        """
        Questo servizio consente di scaricare la ricevuta telematica relativa ad una posizione debitoria.
        Args:
            data (dict): Dati della posizione debitore
        """
        try:
            auth = self._get_auth_token(configuration)
            url = f"{self.base_url}/downloaddocumento"
            headers = RestHeaderClient.X_WWW_FORM_URLENCODED.value

            response = requests.post(url, headers=headers, auth=auth, data=data)
            if response.status_code == 200:
                return response.content.decode("utf-8")
            else:
                log.error(
                    f"Errore nella richiesta. Codice di stato: {response.status_code}",
                    exc_info=DEBUG,
                )
                raise WGolEPayException(response.text)
        except Exception as e:
            log.error(f"Errore durante la richiesta: {e}", exc_info=DEBUG)
            return None

    def download_paper_payment_notice(self, data: dict, configuration: dict):
        """
        Questo servizio consente di scaricare l’avviso di pagamento in formato pdf.
        Args:
            data (dict): Dati della posizione debitore
        """
        try:
            auth = self._get_auth_token(configuration)
            url = f"{self.base_url}/downloadavvisopagamento"
            headers = RestHeaderClient.X_WWW_FORM_URLENCODED.value

            response = requests.post(url, headers=headers, auth=auth, data=data)
            if response.status_code == 200:
                return response.content
            else:
                log.error(
                    f"Errore nella richiesta. Codice di stato: {response.status_code}",
                    exc_info=DEBUG,
                )
                raise WGolEPayException(response.text)
        except Exception as e:
            log.error(f"Errore durante la richiesta: {e}", exc_info=DEBUG)
            return None

    def check_payment_status(self, data: dict, configuration: dict):
        """
        Questo servizio consente di verificare lo stato delle posizione debitoria, già precedentemente
        pubblicate.
         Args:
             data (dict): Dati della posizione debitore
        """
        try:
            auth = self._get_auth_token(configuration)
            url = f"{self.base_url}/elencopsizioni"
            headers = RestHeaderClient.X_WWW_FORM_URLENCODED.value

            response = requests.post(url, headers=headers, auth=auth, data=data)
            if response.status_code == 200:
                return json.loads(response.text)
            else:
                log.error(
                    f"Errore nella richiesta. Codice di stato: {response.status_code}",
                    exc_info=DEBUG,
                )
                raise WGolEPayException(response.text)
        except Exception as e:
            log.error(f"Errore durante la richiesta: {e}", exc_info=DEBUG)
            return None

    def get_payment_url_from_pago_pa(self, data: dict):
        try:
            response = requests.post(self.pago_pa_url, json=data, allow_redirects=False)
            if response.status_code == 302:  # Se la risposta è un redirect
                log.debug(f"Redirect a: {response.headers['Location']}")
                return response.headers["Location"]
            else:
                log.error(
                    f"Errore nella richiesta. Codice di stato: {response.status_code}",
                    exc_info=DEBUG,
                )
                raise WGolEPayException(response.content)
        except Exception as e:
            log.error(f"Errore durante la richiesta: {e}", exc_info=DEBUG)
            return None

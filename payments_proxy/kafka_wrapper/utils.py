import base64
import json
from settings import (
    KAFKA_BOOTSTRAP_SERVERS,
    KAFKA_CONSUMER_GROUP,
)

from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def get_consumer_configuration() -> dict:
    return {
        "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,
        "group.id": KAFKA_CONSUMER_GROUP,
        "auto.offset.reset": "earliest",
    }


def validate_message(message):
    return (
        "remote_id" in message
        and "id" in message
        and "service_id" in message
        and "tenant_id" in message
        and "event_version" in message
        # and "type" in message
    )


def check_configuration_field_to_add(configuration: dict, message: dict) -> dict:
    message["payment"]["type"] = configuration["payment_type"]
    message["payment"]["pagopa_category"] = configuration["category"]
    message["payment"]["due_type"] = configuration["notice_type"]

    if not message["payment"]["receiver"]:
        message["payment"]["receiver"] = configuration["receiver"]

    if not message["payment"]["amount"]:
        message["payment"]["amount"] = configuration["amount"]

    if not message["payment"]["expire_at"]:
        message["payment"]["expire_at"] = configuration["expire_at"]

    if not message["reason"]:
        message["reason"] = configuration["reason"]

    # Converti il dizionario in una stringa JSON ordinata
    dict_string = json.dumps(message)

    # Codifica la stringa JSON in Base64
    message["payment"]["document"] = {
        "hash": base64.b64encode(dict_string.encode()).decode()
    }
    return message

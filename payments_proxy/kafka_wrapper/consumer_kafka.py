from __future__ import annotations
import json
from logging import DEBUG
from confluent_kafka import Consumer
from kafka_wrapper.producer_kafka import produce_message_for_kafka
from kafka_wrapper.utils import (
    check_configuration_field_to_add,
    get_consumer_configuration,
    validate_message,
)
from models.excepitions import JsonException
from models.counter_metrics import CounterMetrics
from models.types import PaymentStatus
from process.payments import WGolEPay
from storage.utils import check_configuration
from settings import (
    KAFKA_CONSUMER_TOPIC,
    EVENT_VERSION,
)

from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def kafka_consumer(counter_metrics: CounterMetrics, wgol_e_pay: WGolEPay):
    log.debug("Start consume")
    # Configura il consumatore Kafka
    consumer_conf = get_consumer_configuration()
    # Crea un consumatore Kafka
    consumer = Consumer(consumer_conf)

    # Sottoscrivi il consumatore alla topic 'test_topic'
    consumer.subscribe([KAFKA_CONSUMER_TOPIC])
    while True:
        try:
            # Leggi i messaggi dalla topic
            msg = consumer.poll(1.0)
            if msg is None:
                continue
            log.debug("Messaggio ricevuto: {}".format(msg.value().decode("utf-8")))
            # Verifica se il valore del messaggio è un JSON valido
            message = json.loads(msg.value().decode("utf-8"))
            if type(message) is not dict:
                raise JsonException
            if msg.error():
                # Fine della partizione, continua la lettura
                log.error(
                    "Errore durante la lettura dei messaggi: {}".format(
                        msg.error().decode("utf-8")
                    ),
                    exc_info=DEBUG,
                )
                continue
            if not validate_message(message):

                log.error(
                    "Messaggio letto non contiene i campi desiderati: {}".format(
                        message
                    ),
                    exc_info=DEBUG,
                )
                continue
            if message["event_version"] != EVENT_VERSION:
                log.debug(
                    f"Messaggio letto contiene una versione non supportata: {message['event_version']}"
                )
                continue
            else:
                log.info(
                    f"Messaggio letto con remote_id: {message['remote_id']} e id : {message['id']}"
                )
                # controllo servizio ok
                configuration = check_configuration(message["service_id"])
                if configuration is None:
                    log.warning(
                        f'Errore durante la lettura della configurazione del servizio: {message["service_id"]}',
                        exc_info=True,
                    )
                    continue
                # controllo tenant ok
                if check_configuration(message["tenant_id"]) is None:
                    log.warning(
                        f'Errore durante la lettura della configurazione del tenant: {message["tenant_id"]}',
                        exc_info=True,
                    )
                    continue
                if not "iuv" in message["payment"]:
                    # qua inserire campi mancanti qual'ora non ci sono prenderli dalla configurazione
                    message = check_configuration_field_to_add(configuration, message)

                processed_event = wgol_e_pay.process_payment(message)

                if processed_event is None:
                    continue
                # Configura il client SOAP
                if processed_event["status"] != PaymentStatus.CREATION_FAILED.value:
                    counter_metrics.increment_success()
                produce_message_for_kafka(processed_event, True, counter_metrics)
        except JsonException as e:
            log.error("Il messaggio non è un JSON valido", exc_info=DEBUG)
            counter_metrics.increment_failure()
            counter_metrics.inc_validation_error_payment_metrics()
            continue  # Salta al prossimo messaggio
        except KeyboardInterrupt:
            counter_metrics.increment_failure()
            pass
        except Exception as e:
            log.error(f"Errore durante processamento {e}", exc_info=DEBUG)
            counter_metrics.increment_failure()

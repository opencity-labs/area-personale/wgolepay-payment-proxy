import datetime
import json
from logging import DEBUG
import uuid
from confluent_kafka import Producer
from models.counter_metrics import CounterMetrics
from storage.utils import save_events
from settings import KAFKA_BOOTSTRAP_SERVERS, KAFKA_PRODUCER_TOPIC, APP_NAME, VERSION

from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def delivery_report(err, msg):
    """Callback function called on producing messages"""
    if err is not None:
        log.error("Message delivery failed: {}".format(err), exc_info=DEBUG)
    else:
        log.debug("Message delivered to {} [{}]".format(msg.topic(), msg.partition()))


def produce_message(topic: str, key: str, message: str):
    # Configura il produttore Kafka
    producer_conf = {
        "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,  # Utilizza questo se il produttore è in un contenitore Docker
    }

    # Crea un produttore Kafka
    producer = Producer(producer_conf)
    producer.produce(topic, key=key, value=message, callback=delivery_report)
    producer.poll(1)

    log.debug("Message sent to Kafka")


def produce_message_for_kafka(
    message: dict, status: bool, counter_metrics: CounterMetrics
):
    message["event_id"] = str(uuid.uuid4())
    message["event_created_at"] = (
        datetime.datetime.now().replace(microsecond=0).astimezone().isoformat()
    )
    message["app_id"] = f"{APP_NAME}:{VERSION}"
    json_data = json.dumps(message)
    if message:
        event_key = (
            message["service_id"]
            if "service_id" in message
            else "00000000-0000-0000-0000-000000000000"
        )
        if status:
            counter_metrics.increment_success()
        else:
            counter_metrics.increment_failure()
        produce_message(KAFKA_PRODUCER_TOPIC, event_key, json_data)
        log.info(f"Message produced in: {KAFKA_PRODUCER_TOPIC} with key: {event_key}")
        save_events(message["id"], message)
    else:
        log.error("No message received", exc_info=DEBUG)

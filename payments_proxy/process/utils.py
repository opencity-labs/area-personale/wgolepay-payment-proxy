import requests
import xmltodict
from jinja2 import Template
from io import BytesIO
from oc_python_sdk.models.payment import Payment
from models.excepitions import WGolEPayException
from models.types import StatusPayment, WGolEPayCodeSoggetto, WGolEPayTipoSoggetto
from models.logger import get_logger
from storage.utils import check_events
from settings import GOTENBERG_URL, DEBUG

logger = get_logger()


def get_check_request(payment: Payment, tenant: dict):
    logger.debug(
        f"Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request"
    )

    return {
        "codente": tenant["cod_ente"],
        "codconv": tenant["cod_conv"],
        "pagenumber": 0,
        "rowforpage": 1,
        "iuv": payment.payment.iuv,
        "anagraficapagatore": "",
        "identificativopagatore": "",
    }


def get_creation_request(
    payment: Payment, tenant: dict, config: dict, landing_url: str
) -> dict:
    logger.debug(
        f"Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request"
    )
    subject_code = subject_type = ""
    if payment.payer.type == WGolEPayTipoSoggetto.TYPE_HUMAN.value:
        subject_type = WGolEPayTipoSoggetto.TYPE_HUMAN.value
        subject_code = WGolEPayCodeSoggetto.FISCAL_CODE.value
    else:
        subject_type = WGolEPayTipoSoggetto.TYPE_LEGAL.value
        subject_code = WGolEPayCodeSoggetto.VAT_CODE.value
    cod_cont = config["category"].split("/")[1]
    return {
        "codente": tenant["cod_ente"],
        "codconv": tenant["cod_conv"],
        "urlok": f"{landing_url}?status_payment={StatusPayment.OK.value}",
        "urlko": f"{landing_url}?status_payment={StatusPayment.KO.value}",
        "listpd": [
            {
                "codente": tenant["cod_ente"],
                "codconv": tenant["cod_conv"],
                "idbeneficiario": tenant["tax_identification_number"],
                "rata": "",
                "iuv": "",
                "importo": payment.payment.amount,
                "causale": payment.reason,
                "datascadenza": payment.payment.expire_at.strftime("%Y-%m-%d"),
                "tipocontabilita": "9",
                "codicecontabilita": cod_cont,
                "codiceservizio": config["cod_service"],
                "tiporifercreditore": config["notice_type"],
                "codicerifercreditore": payment.payment.iud,
                "idutenza": payment.payment.iud,
                "nav": "",
                "tipodebitore": subject_type,
                "tipocoddebitore": subject_code,
                "idfiscale": payment.payer.tax_identification_number,
                "anagraficadebitore": f"{payment.payer.name} {payment.payer.family_name}",
                "indirizzodebitore": payment.payer.street_name or None,
                "civicodebitore": payment.payer.building_number or None,
                "capdebitore": payment.payer.postal_code or None,
                "localitadebitore": payment.payer.town_name or None,
                "provinciadebitore": payment.payer.country_subdivision or None,
                "nazionedebitore": payment.payer.country or None,
                "emaildebitore": payment.payer.email or None,
                "telefonodebitore": None,
                "nomeufficio": "",
                "spontaneo": "S",
            }
        ],
    }


def get_payment(payment_id: str) -> Payment:
    payment = check_events(payment_id)
    if not payment:
        raise WGolEPayException(f"Missing payment {payment_id}")
    payment = Payment(**payment)
    return payment


def generate_pdf_from_binary(binary_data):
    with BytesIO() as pdf_buffer:
        pdf_buffer.write(binary_data)
        pdf_buffer.seek(0)  # Reset buffer position to start
        return pdf_buffer.getvalue()


def get_pago_pa_payment_payload(payment: Payment, tenant: dict, landing_url: str):
    return {
        "emailNotice": payment.payer.email,
        "paymentNotices": [
            {
                "noticeNumber": payment.payment.notice_code,
                "fiscalCode": tenant["tax_identification_number"],
                "amount": payment.payment.amount * 100,
                "companyName": tenant["name"],
                "description": payment.reason,
            },
        ],
        "returnUrls": {
            "returnOkUrl": f"{landing_url}?status_payment={StatusPayment.OK.value}",
            "returnCancelUrl": f"{landing_url}?status_payment={StatusPayment.KO.value}",
            "returnErrorUrl": f"{landing_url}?status_payment={StatusPayment.KO.value}",
        },
    }


def _gotenberg_request(content):
    """
    Method for handling possible errors that
    may be encountered in Gotenberg request
    :param content: content to be generated using Gotenberg
    :return: Gotenberg response content
    """
    try:
        response = requests.post(
            url=GOTENBERG_URL,
            files={"file": ("index.html", str(content))},
            data={
                "marginTop": 0,
                "marginLeft": 0,
                "marginBottom": 0,
                "marginRight": 0,
            },
        )
        return response.content
    except requests.exceptions.ConnectionError as ce:
        logger.error(
            f"An error occurred: unable to connect to url {GOTENBERG_URL}",
            exc_info=DEBUG,
        )
    except requests.exceptions.ReadTimeout as rt:
        logger.error(
            f"An error occurred: unable to read from url {GOTENBERG_URL}",
            exc_info=DEBUG,
        )
    except Exception as ex:
        logger.error(
            f"An error occurred from url {GOTENBERG_URL}: {str(ex)}", exc_info=DEBUG
        )
    raise WGolEPayException("An error occurred: cannot generate PDF document")


def generate_pdf_from_xml(xml_response):
    try:
        xml_dict = xmltodict.parse(
            xml_response,
            process_namespaces=True,
            namespaces={"http://www.digitpa.gov.it/schemas/2011/Pagamenti/": None},
        )
        logger.debug(f"XML response: {xml_dict}")
        rendered_html = _get_receipt_html_from_xml(xml_dict)
        # Genera il PDF dall'HTML
        logger.debug(f"HTML response: {rendered_html}")
        pdf_data = _gotenberg_request(rendered_html)
        return pdf_data
    except Exception as ex:
        logger.error(f"Error generating PDF from XML: {ex}", exc_info=DEBUG)


def _get_receipt_html_from_xml(xml_dict: dict):
    html_template = _get_file_template("receipt_template")
    template = Template(html_template)
    rendered_html = template.render(
        anagraficaPagatore=xml_dict["CtReceiptV2"]["debtor"]["fullName"],
        denominazioneAttestante=xml_dict["CtReceiptV2"]["PSPCompanyName"],
        identificativoMessaggioRicevuta=xml_dict["CtReceiptV2"]["receiptId"],
        codiceIdentificativoUnivocoDebitore=xml_dict["CtReceiptV2"]["debtor"][
            "uniqueIdentifier"
        ]["entityUniqueIdentifierValue"],
        identificativoUnivocoVersamento=xml_dict["CtReceiptV2"]["creditorReferenceId"],
        dataEsitoSingoloPagamento=xml_dict["CtReceiptV2"]["transferDate"],
        causaleVersamento=xml_dict["CtReceiptV2"]["transferList"]["transfer"][
            "remittanceInformation"
        ],
        denominazioneBeneficiario=xml_dict["CtReceiptV2"]["companyName"],
        codiceIdentificativoUnivocoCreditore=xml_dict["CtReceiptV2"]["fiscalCode"],
        importoTotalePagato=xml_dict["CtReceiptV2"]["paymentAmount"],
        metodoPagamento=xml_dict["CtReceiptV2"]["paymentMethod"],
        fees=xml_dict["CtReceiptV2"]["fee"],
    )

    return rendered_html


def _get_file_template(file_name):
    with open(f"process/template/{file_name}.html", "r") as f:
        return f.read()

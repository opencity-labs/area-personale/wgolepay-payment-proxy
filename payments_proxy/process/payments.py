import json
from process.utils import (
    generate_pdf_from_binary,
    generate_pdf_from_xml,
    get_check_request,
    get_creation_request,
    get_pago_pa_payment_payload,
    get_payment,
)
from storage.utils import (
    check_events,
    get_all_file_configuration,
    save_events,
)
from settings import DEBUG
from models.counter_metrics import CounterMetrics
from models.excepitions import WGolEPayException
from rest.rest_client import RestClient
from models.logger import get_logger
from models.types import (
    WGolEPayPaymentStatus,
    LinkType,
)
from oc_python_sdk.models.payment import (
    HttpMethodType,
    Payment,
    PaymentStatus,
)


logger = get_logger()


class WGolEPay:
    def __init__(
        self,
        counter_metrics: CounterMetrics,
        app_online_url: str,
        app_landing_url: str,
        app_notice_url: str,
        app_receipt_url: str,
        app_update_url: str,
    ):
        logger.info("Starting WGolEPay initialization")

        self.tenants = {}
        self.configs = {}
        self.rest_client = RestClient()
        self.counter_metrics = counter_metrics
        self.app_online_url: str = app_online_url
        self.app_notice_url: str = app_notice_url
        self.app_receipt_url: str = app_receipt_url
        self.app_update_url: str = app_update_url
        self.app_landing_url: str = app_landing_url

        self._load_tenants_and_configs()

        logger.info("WGolEPay initialization completed")

    def process_payment(self, event, **kwargs):
        payment = Payment(**event)
        if self._is_external(payment):  # evento è proveniente da una fonte esterna
            logger.info(f"Event {event['id']} from external source: Processing event")
            payment.links.update.url = self.app_update_url.replace(
                "{id}", str(payment.id)
            )
            payment.links.update.method = HttpMethodType.HTTP_METHOD_GET

            save_events(str(payment.id), event)
            return json.loads(payment.json())
        if payment.is_payment_creation_needed():  # flusso da kakfa
            logger.info(
                f"Event {event['id']} from application {event['remote_id']}: Processing event"
            )
            return self._create_payment(event)
        else:
            logger.info(
                f"Event {event['id']} from application {event['remote_id']}: Processing not required"
            )
            return None

    def _is_external(self, payment: Payment):
        """
        :param payment:
        :return: bool
        Checks if a payment was processed by the proxy
        or if it was imported from an external source
        """
        try:
            if (
                payment.status == PaymentStatus.STATUS_PAYMENT_PENDING
                and not check_events(payment.id)
            ):
                self._get_configs_by_identifier(config_id=str(payment.service_id))
                return True
        except WGolEPayException:
            return False
        return False

    def _create_payment(self, event):
        payment = Payment.parse_obj(event)
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        config = self._get_configs_by_identifier(str(payment.service_id))
        logger.info(
            f"Event {payment.id} from application {payment.remote_id}: Prepairing payment creation request"
        )

        payment_id = str(payment.id)
        landing_url = self.get_landing_url(
            payment_id=payment_id, status_payment=None, url_type=LinkType.INTERNAL_LINK
        )

        payload = get_creation_request(payment, tenant, config, landing_url)

        try:
            logger.debug(
                f"Event {payment.id} from application {payment.remote_id}: "
                f"Sending creation request with data {payload}",
            )

            response = self.rest_client.create_debtor_position_and_return_payment_url(
                payload, tenant
            )
            if not response:
                raise WGolEPayException("Empty response from WS")
            # Aggiornamento informazioni pagamento restituite dal ws
            payment.payment.iuv = response["liuv"][0]["iuv"]
            payment.payment.notice_code = response["liuv"][0]["nav"]

            # Link pagamento online
            payment.links.online_payment_begin.url = (
                self.get_online_url(payment_id, url_type=LinkType.INTERNAL_LINK)
                + f"?url_response={response['urllocation']}"
            )

            payment.links.online_payment_begin.method = HttpMethodType.HTTP_METHOD_GET

            # Link per scaricare l'avviso di pagamento pdf (Pagamento offline)  # noqa: W505
            payment.links.offline_payment.url = self.app_notice_url.replace(
                "{id}", payment_id
            )
            payment.links.offline_payment.method = HttpMethodType.HTTP_METHOD_GET

            # Link per scaricare la ricevuta di pagamento disponibile SOLO a pagamento effettuato  # noqa: W505
            payment.links.receipt.url = self.app_receipt_url.replace("{id}", payment_id)
            payment.links.receipt.method = HttpMethodType.HTTP_METHOD_GET

            # Link per richiedere l'aggiornamento dello stato del pagamento  # noqa: W505
            payment.links.update.url = self.app_update_url.replace("{id}", payment_id)
            payment.links.update.method = HttpMethodType.HTTP_METHOD_GET

            payment.status = PaymentStatus.STATUS_PAYMENT_PENDING.value
            payment = payment.update_time("links.update.next_check_at")
        except WGolEPayException as ex:
            self.counter_metrics.inc_internal_error_payment_metrics()
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
        except Exception as ex:
            self.counter_metrics.inc_internal_error_payment_metrics()
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )

        payment = payment.update_time("updated_at")

        return json.loads(payment.json())

    def get_landing_url(
        self,
        payment_id: str,
        status_payment: str = None,
        url_type: LinkType = LinkType.EXTERNAL_LINK,
    ):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_landing_url.replace("{id}", payment_id)
        payment: Payment = get_payment(payment_id)
        if status_payment == "OK":
            payment.status = PaymentStatus.STATUS_COMPLETE
        payment = payment.update_time("links.online_payment_landing.last_opened_at")
        payment = payment.update_time("updated_at")
        return payment.links.online_payment_landing.url, json.loads(payment.json())

    def get_online_url(
        self, payment_id: str, url_type: LinkType = LinkType.EXTERNAL_LINK
    ):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_online_url.replace("{id}", payment_id)
        payment: Payment = get_payment(payment_id)

        online_url = self.call_pago_pa_for_get_online_url(payment)

        payment = self.update_online_payment_begin(payment)
        return online_url, json.loads(payment.json())

    def call_pago_pa_for_get_online_url(self, payment: Payment):
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        landing_url = self.get_online_url(
            str(payment.tenant_id), url_type=LinkType.INTERNAL_LINK
        )
        payload_for_pago_pa = get_pago_pa_payment_payload(payment, tenant, landing_url)
        try:
            logger.debug(
                f"Event {str(payment.tenant_id)} from application {payment.remote_id}: "
                f"Sending pago pa request with data {payload_for_pago_pa}",
            )
            response = self.rest_client.get_payment_url_from_pago_pa(
                payload_for_pago_pa
            )
            return response
        except WGolEPayException as ex:
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}"
            )

    def update_online_payment_begin(self, payment: Payment):
        payment = payment.update_time("links.online_payment_begin.last_opened_at")
        payment = payment.update_time("updated_at")
        return payment

    def check_for_updates(self, payment_id: str):
        payment: Payment = get_payment(payment_id)

        if payment.status == PaymentStatus.STATUS_COMPLETE:
            logger.debug(f"Payment {payment.id} already completed")
            return json.loads(payment.json()), False

        try:
            logger.debug(f"Payment {payment.id}: Sending search request")
            tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
            payload = get_check_request(payment, tenant)
            response = self.rest_client.check_payment_status(payload, tenant)

            if "resultList" in response and response["resultList"]:
                response = response["resultList"][0]
                if response["stato"] in [
                    WGolEPayPaymentStatus.CHIUSA.value,
                    WGolEPayPaymentStatus.RENDICONTATA.value,
                ]:
                    logger.info(
                        f"Payment {payment.payment.iuv} from application {payment.remote_id}: "
                        f'Callback with status {response["stato"]}',
                    )
                    payment.payment.transaction_id = response[
                        "identificativoUnivocoVersamento"
                    ]
                    payment.status = PaymentStatus.STATUS_COMPLETE
                elif response["stato"] == WGolEPayPaymentStatus.APERTA.value:
                    payment.status = PaymentStatus.STATUS_PAYMENT_STARTED
                    logger.debug(f"Payment {payment.id} has not been paid yet")
                else:
                    logger.debug(f"Payment {payment.id} has not been paid yet")
                payment = payment.update_time("links.update.last_check_at")
                payment = payment.update_time("updated_at")
                payment = payment.update_check_time()
                return json.loads(payment.json()), True
        except WGolEPayException as ex:
            self.counter_metrics.inc_internal_error_payment_metrics()
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
        except Exception as ex:
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )

    def get_notice_file(self, payment_id: str):
        payment: Payment = get_payment(payment_id)
        try:
            tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
            payload = {
                "codente": tenant["cod_ente"],
                "codconv": tenant["cod_conv"],
                "iuv": payment.payment.iuv,
            }
            logger.debug(
                f"Payment {payment.payment.iuv} from application {payment.remote_id}: "
                f"Sending offline payment request with data {payload}",
            )

            payment = payment.update_time("links.offline_payment.last_opened_at")
            payment = payment.update_time("updated_at")
            response = self.rest_client.download_paper_payment_notice(payload, tenant)
            return generate_pdf_from_binary(response), json.loads(payment.json())
        except WGolEPayException as ex:
            self.counter_metrics.inc_internal_error_payment_metrics()
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
        except Exception as ex:
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
            raise WGolEPayException

    def get_receipt_file(self, payment_id: str):
        payment: Payment = get_payment(payment_id)
        try:
            tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
            payload = {
                "codente": tenant["cod_ente"],
                "codconv": tenant["cod_conv"],
                "tipodoc": "RT",
                "iuv": payment.payment.iuv,
            }
            logger.debug(
                f"Payment {payment.payment.iuv} from application {payment.remote_id}: "
                f"Sending offline payment request with data {payload}",
            )

            payment = payment.update_time("links.offline_payment.last_opened_at")
            payment = payment.update_time("updated_at")
            xml_response = self.rest_client.download_paper_payment_document(
                payload, tenant
            )
            return generate_pdf_from_xml(xml_response), json.loads(payment.json())
        except WGolEPayException as ex:
            self.counter_metrics.inc_internal_error_payment_metrics()
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
        except Exception as ex:
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(
                f"Event {payment.id} from application {payment.remote_id}: Error: {ex}",
                exc_info=DEBUG,
            )
            raise WGolEPayException("Receipt not available")

    def _get_tenant_by_identifier(self, tenant_id: str):
        if tenant_id in self.tenants:
            return self.tenants[tenant_id]
        logger.warning(f"Missing configuration for tenant identifier {tenant_id}")
        raise WGolEPayException("Tenant configuration missing")

    def _get_configs_by_identifier(self, config_id: str):
        if config_id in self.configs:
            return self.configs[config_id]
        logger.warning(f"Missing configuration for config identifier {config_id}")
        raise WGolEPayException("Configuration missing")

    def _load_tenants_and_configs(self):
        for configuration in get_all_file_configuration():
            self.update_configurations(configuration)
        logger.debug(self.tenants)
        logger.debug(self.configs)

    def update_configurations(self, configuration: dict):
        if configuration["active"]:
            if "tenant_id" in configuration:
                self.configs[configuration["id"]] = configuration
            else:
                self.tenants[configuration["id"]] = configuration

    def soft_delete_configurations(self, configuration: dict):
        if "tenant_id" in configuration:
            self.configs.pop(configuration["id"])
        else:
            self.tenants.pop(configuration["id"])

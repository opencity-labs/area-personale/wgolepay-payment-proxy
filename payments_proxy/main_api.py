import asyncio
import json
from logging import DEBUG

import sentry_sdk
from threading import Thread
from confluent_kafka.admin import AdminClient
from confluent_kafka import KafkaError
from prometheus_client import REGISTRY, generate_latest
from fastapi.responses import (
    PlainTextResponse,
    RedirectResponse,
    JSONResponse,
)
from fastapi import FastAPI, Query, Response, status
from fastapi.openapi.utils import get_openapi
from storage.utils import (
    check_configuration,
    disable_configuration,
    read_local_schema,
    save_configuration,
)
from models.logger import get_logger
from models.api import (
    Configuration,
    ConfigurationPatch,
    ConfigurationPut,
    ConfigurationTenant,
    ConfigurationTenantPatch,
    ConfigurationTenantPut,
    ErrorMessage,
    JSONResponseCustom,
    Status,
    ERR_422_SCHEMA,
)
from models.types import MethodApi, SchemaType
from models.counter_metrics import CounterMetrics
from models.excepitions import NotExistingException, ExistException, WGolEPayException
from kafka_wrapper.consumer_kafka import kafka_consumer
from kafka_wrapper.producer_kafka import produce_message_for_kafka
from process.payments import WGolEPay
from process.utils import get_payment
from settings import (
    APP_NAME,
    SENTRY_ENABLED,
    SENTRY_DSN,
    KAFKA_BOOTSTRAP_SERVERS,
    KAFKA_CONSUMER_TOPIC,
    PREFIX_API,
    EXTERNAL_API_URL,
    INTERNAL_API_URL,
    VERSION,
)
from oc_python_sdk.models.payment import Payment
from storage.storage_manager import StorageManager

app = FastAPI(
    docs_url=PREFIX_API + "docs",
    openapi_url=PREFIX_API + "openapi.json",
    responses={422: ERR_422_SCHEMA},
)
storage_manager = StorageManager()

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()

counter_metrics = CounterMetrics()
if SENTRY_ENABLED:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
        # Set profiles_sample_rate to 1.0 to profile 100%
        # of sampled transactions.
        # We recommend adjusting this value in production.
        profiles_sample_rate=1.0,
    )


# Endpoint per esporre le metriche in formato Prometheus
@app.get(
    "/metrics",
    response_class=PlainTextResponse,
    description="Metriche Prometheus",
    tags=["Metrics"],
)
def metrics():
    try:
        return generate_latest(REGISTRY)
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        log.error(
            f"An unexpected error occurred during the health check: {str(e)}",
            exc_info=DEBUG,
        )
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"An unexpected error occurred during the health check: {str(e)}",
            MethodApi.GET,
        )


@app.get(
    "/status",
    tags=["Status"],
    description="Get Status",
    status_code=200,
    response_class=JSONResponseCustom,
    responses={
        503: {"model": ErrorMessage, "description": "Config Unavailable"},
        200: {"model": Status, "description": "Successful Response"},
    },
)
def get_health_kafka_status():
    try:
        consumer_conf = {
            "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,
        }

        # Crea un consumatore Kafka
        admin_client = AdminClient(consumer_conf)
        topics = admin_client.list_topics(timeout=5).topics
        log.debug(f"Topics: {topics}")

        if not topics:
            return _error_response(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "No topics found",
                "No topics found. Health check failed",
                MethodApi.GET,
            )
        elif KAFKA_CONSUMER_TOPIC not in topics:
            return _error_response(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "Topic not found",
                f"Expected topic '{KAFKA_CONSUMER_TOPIC}' not found. Health check failed",
                MethodApi.GET,
            )

        log.debug("Health check passed")
        status_code = status.HTTP_200_OK
        counter_metrics.increment_api_requests_total(status_code, MethodApi.GET)
        return JSONResponse(
            status_code=status_code,
            content={"status": "ok"},
        )
    except KafkaError as ke:
        log.error(f"Health check failed: {ke}", exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"Health check failed: {ke}",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )

    except Exception as e:
        log.error(
            f"An unexpected error occurred during the health check: {str(e)}",
            exc_info=DEBUG,
        )
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Health check failed",
            f"An unexpected error occurred during the health check: {str(e)}",
            MethodApi.GET,
        )


@app.get(
    "/online-payment/{payment_id}",
    description="Get Online Url",
    tags=["Payments"],
)
async def get_online_payment_url(payment_id: str, url_response: str = Query(None)):
    try:
        if url_response:
            payment = wgol_e_pay.update_online_payment_begin(get_payment(payment_id))
            payment = json.loads(payment.json())
            payment_url = url_response
        else:
            payment_url, payment = wgol_e_pay.get_online_url(payment_id)
            if payment_url is None:
                raise WGolEPayException("Error getting online payment url")

        log.debug(f"Payment url: {payment_url}, payment: {payment}")
        produce_message_for_kafka(payment, False, counter_metrics)
        counter_metrics.increment_api_requests_total(
            status.HTTP_307_TEMPORARY_REDIRECT, MethodApi.GET
        )
        return RedirectResponse(payment_url)
    except WGolEPayException as wpe:
        # Log the error for debugging purposes
        log.debug(f"Error payments notice: {wpe}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Item not found",
            "Item not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error handling request get_online_payment_url:{e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.get(
    "/notice/{payment_id}",
    description="Get Notice file",
    tags=["Payments"],
)
async def get_notice_file(payment_id: str):
    try:
        notice_pdf, payment = wgol_e_pay.get_notice_file(payment_id)
        log.debug(f"Notice pdf: {notice_pdf}, payment: {payment}")
        produce_message_for_kafka(payment, False, counter_metrics)
        counter_metrics.increment_api_requests_total(status.HTTP_200_OK, MethodApi.GET)
        return Response(notice_pdf, media_type="application/pdf")
    except WGolEPayException as wpe:
        # Log the error for debugging purposes
        log.debug(f"Error payments notice: {wpe}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Item not found",
            "Item not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error handling request get_notice_file:{e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.get(
    "/receipt/{payment_id}",
    description="Get Receipt file",
    tags=["Payments"],
)
async def get_receipt_file(payment_id: str):
    try:
        receipt_xml, payment = wgol_e_pay.get_receipt_file(payment_id)
        if not receipt_xml:
            raise WGolEPayException()
        log.debug(f"Receipt pdf: {receipt_xml}, payment: {payment}")
        # produce_message_for_kafka(payment, False, counter_metrics)
        counter_metrics.increment_api_requests_total(status.HTTP_200_OK, MethodApi.GET)
        return Response(receipt_xml, media_type="application/pdf")
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except WGolEPayException as wpe:
        # Log the error for debugging purposes
        log.debug(f"Error payments receipt: {wpe}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Item not found",
            "Item not found",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error handling request get_receipt_file:{e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.get(
    "/update/{payment_id}",
    description="Check and Update Payment Status",
    tags=["Payments"],
)
async def check_for_updates(payment_id: str):
    try:
        payment, status_changed = wgol_e_pay.check_for_updates(payment_id)
        log.debug(f"Payment: {payment}, status_changed: {status_changed}")
        if status_changed:
            produce_message_for_kafka(payment, True, counter_metrics)
        counter_metrics.increment_api_requests_total(status.HTTP_200_OK, MethodApi.GET)
        return JSONResponse(content=payment)
    except WGolEPayException as wpe:
        # Log the error for debugging purposes
        log.debug(f"Error payments update: {wpe}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Item not found",
            "Item not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error handling request check_for_updates:{e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.get(
    "/landing/{payment_id}",
    description="Redirect User to Landing Page",
    tags=["Payments"],
)
async def get_landing_url(payment_id: str, status_payment: str = Query(None)):
    try:
        redirect_url, payment = wgol_e_pay.get_landing_url(
            payment_id=payment_id, status_payment=status_payment
        )
        log.debug(f"Redirect url: {redirect_url}, payments:{payment}")
        produce_message_for_kafka(payment, True, counter_metrics)
        counter_metrics.increment_api_requests_total(
            status.HTTP_307_TEMPORARY_REDIRECT, MethodApi.GET
        )
        if status_payment:
            redirect_url = redirect_url + f"?payment={status_payment.lower()}"
        return RedirectResponse(redirect_url)
    except WGolEPayException as wpe:
        # Log the error for debugging purposes
        log.debug(f"Error payments redirect: {wpe}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Item not found",
            "Item not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error handling request get_landing_url:{e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.get(
    "/tenants/schema",
    description="Get Tenant Form Schema",
    tags=["Schema", "Tenants"],
)
def get_tenant_form_schema():
    try:
        data = read_local_schema(SchemaType.TENANT)
        return JSONResponse(content=json.loads(data))

    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error reading schema tenants: {e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


# Endpoint to retrieve a tenant by ID
@app.get(
    "/tenants/{tenant_id}", description="Get Tenant Configuration", tags=["Tenants"]
)
async def get_tenant(tenant_id: str):
    try:
        tenants_data = check_configuration(tenant_id)
        # Check if the tenant_id exists in the data
        if tenants_data:
            return JSONResponse(content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.post(
    "/tenants",
    description="Create New Tenant Configuration",
    status_code=status.HTTP_201_CREATED,
    tags=["Tenants"],
)
async def save_tenant_configuration(config_tenant: ConfigurationTenant):
    try:
        if not config_tenant.active:
            raise ValueError
        configuration_existing=check_configuration(config_tenant.id)
        if configuration_existing and configuration_existing["active"]:
            raise ExistException
        # Save the tenant to the local file
        tenant = config_tenant.to_dict()
        save_configuration(tenant)
        wgol_e_pay.update_configurations(tenant)
        log.debug("Dati ricevuti con successo")

        # Return a response with status code 201 and no response body
        status_code = status.HTTP_201_CREATED
        counter_metrics.increment_api_requests_total(status_code)
        return JSONResponse(
            status_code=status_code,
            content=tenant,
        )

    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.POST,
        )
    except ExistException as ee:
        # Log the error for debugging purposes
        log.debug(f"Existing tenants: {config_tenant}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_409_CONFLICT,
            "Existing tenants",
            f"The tenants with id:{config_tenant.id} is already exist",
            MethodApi.POST,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Errore nella gestione della richiesta:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.POST,
        )


@app.put(
    "/tenants/{tenant_id}",
    description="Upddate Tenant Configuration",
    tags=["Tenants"],
)
async def update_tenant(tenant_id: str, updated_tenant: ConfigurationTenantPut):
    try:
        if not updated_tenant.active:
            raise ValueError
        # Retrieve the tenant data from the local file
        tenants_data = check_configuration(tenant_id)
        # Check if the tenant_id exists in the data
        if tenants_data:
            # Update the tenant data with the new values
            tenants_data = updated_tenant.to_dict()
            # Save the updated tenant data to the local file
            tenants_data["id"] = tenant_id
            save_configuration(tenants_data)
            wgol_e_pay.update_configurations(tenants_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code)
            return JSONResponse(status_code=status_code, content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.PUT,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PUT,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PUT,
        )


# Endpoint to update a Tenants by ID with patch
@app.patch(
    "/tenants/{tenant_id}",
    description="Update Existing Tenants Configuration",
    tags=["Tenants"],
)
async def update_tenant_patch(tenant_id: str, updated_tenant: ConfigurationTenantPatch):
    try:
        if not updated_tenant.active:
            raise ValueError
        tenants_data = check_configuration(tenant_id)
        # Check if the id exists in the data
        if tenants_data:
            update_dict = updated_tenant.to_dict()
            for key, value in update_dict.items():
                if key in tenants_data and value:
                    tenants_data[key] = value
                else:
                    log.debug(f"Key:{key} o value:{value} non presente")
            # Save the updated config data to the local file
            save_configuration(tenants_data)
            wgol_e_pay.update_configurations(tenants_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code)
            return JSONResponse(status_code=status_code, content=tenants_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.PATCH,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PATCH,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PATCH,
        )


# Endpoint to delete a tenant by ID
@app.delete(
    "/tenants/{tenant_id}",
    description="Delete Tenant",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["Tenants"],
)
async def delete_tenant(tenant_id: str):
    try:
        tenants_data = check_configuration(tenant_id)

        # Check if the tenant_id exists in the data
        if tenants_data:
            # Delete the tenant data
            disable_configuration(tenants_data)
            wgol_e_pay.soft_delete_configurations(tenants_data)
            log.debug(f"Tenant {tenant_id}.json disabled successfully")
            status_code = status.HTTP_204_NO_CONTENT
            counter_metrics.increment_api_requests_total(status_code)
            return JSONResponse(status_code=status_code, content={})
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Tenant {tenant_id} not exist or disabled", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Tenant not found",
            "Tenant not found",
            MethodApi.DELETE,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.DELETE,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.DELETE,
        )


@app.get(
    "/configs/schema",
    description="Get Config Form Schema",
    tags=["Schema", "Configs"],
)
def get_config_form_schema():
    try:
        data = read_local_schema(SchemaType.CONFIG)
        return JSONResponse(content=json.loads(data))

    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Log the error for debugging purposes
        log.debug(f"Error reading schema: {e}")
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


# Endpoint to retrieve a configs by ID
@app.get(
    "/configs/{config_id}",
    description="Get Config Configuration",
    tags=["Configs"],
)
async def get_config(config_id: str):
    try:
        config_data = check_configuration(config_id)
        # Check if the id exists in the data
        if config_data:
            counter_metrics.increment_api_requests_total(
                status.HTTP_200_OK, MethodApi.GET
            )
            return JSONResponse(content=config_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Config {config_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Config not found",
            "Config not found",
            MethodApi.GET,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.GET,
        )
    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.GET,
        )


@app.post(
    "/configs",
    description="Save Config Configuration",
    status_code=status.HTTP_201_CREATED,
    tags=["Configs"],
)
async def save_config_configuration(configuration_config: Configuration):
    try:
        if not configuration_config.active:
            raise ValueError
        configuration_existing = check_configuration(configuration_config.id)
        if configuration_existing and configuration_existing["active"]:
            raise ExistException
        config = configuration_config.to_dict()
        # Salvare i dati nei settings
        save_configuration(config)
        wgol_e_pay.update_configurations(config)
        # Restituisci una risposta al client
        log.debug("Dati ricevuti con successo")
        status_code = status.HTTP_201_CREATED
        counter_metrics.increment_api_requests_total(status_code, MethodApi.POST)
        return JSONResponse(
            status_code=status_code,
            content=config,
        )

    except ExistException as ee:
        # Log the error for debugging purposes
        log.debug(f"Existing configs: {configuration_config}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_409_CONFLICT,
            "Existing configs",
            f"The configs with id:{configuration_config.id} is already exist",
            MethodApi.POST,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.POST,
        )
    except Exception as e:
        # Gestisci eventuali errori qui
        log.error("Errore nella gestione della richiesta:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.POST,
        )


# Endpoint to update a configs by ID
@app.put(
    "/configs/{config_id}",
    description="Update Config Configuration",
    tags=["Configs"],
)
async def update_config(config_id: str, updated_config: ConfigurationPut):
    try:
        if not updated_config.active:
            raise ValueError
        # Retrieve the config data from the local file
        configs_data = check_configuration(config_id)
        # Check if the id exists in the data
        if configs_data:
            tenant_id = configs_data["tenant_id"]
            # Update the config data with the new values
            configs_data = updated_config.to_dict()
            # Save the updated config data to the local file
            configs_data["id"] = config_id
            configs_data["tenant_id"] = tenant_id
            save_configuration(configs_data)
            wgol_e_pay.update_configurations(configs_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PUT)
            return JSONResponse(status_code=status_code, content=configs_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Config {config_id} not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Config not found",
            "Config not found",
            MethodApi.PUT,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PUT,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PUT,
        )


# Endpoint to update a configs by ID with patch
@app.patch(
    "/configs/{config_id}",
    description="Update Existing Config Configuration",
    tags=["Configs"],
)
async def update_config_patch(config_id: str, updated_config: ConfigurationPatch):
    try:
        if not updated_config.active:
            raise ValueError
        # Retrieve the config data from the local file
        configs_data = check_configuration(config_id)
        # Check if the id exists in the data
        if configs_data:
            update_dict = updated_config.to_dict()
            for key, value in update_dict.items():
                if key in configs_data and value:
                    configs_data[key] = value
                else:
                    log.debug(f"Key:{key} o value:{value} non presente")
            # Save the updated config data to the local file
            save_configuration(configs_data)
            wgol_e_pay.update_configurations(configs_data)
            status_code = status.HTTP_201_CREATED
            counter_metrics.increment_api_requests_total(status_code, MethodApi.PATCH)
            return JSONResponse(status_code=status_code, content=configs_data)
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Config {config_id} not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Config not found",
            "Config not found",
            MethodApi.PATCH,
        )
    except ValueError as ve:
        # Log the error for debugging purposes
        log.debug(f"Validation error: {ve}")
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.PATCH,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.PATCH,
        )


# Endpoint to delete a config by ID
@app.delete(
    "/configs/{config_id}",
    description="Delete Config",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["Configs"],
)
async def delete_config(config_id: str):
    try:
        config_data = check_configuration(config_id)

        # Check if the id exists in the data
        if config_data:
            # Delete the config data
            disable_configuration(config_data)
            wgol_e_pay.soft_delete_configurations(config_data)
            log.debug(f"Config {config_id}.json deleted successfully")
            status_code = status.HTTP_204_NO_CONTENT
            counter_metrics.increment_api_requests_total(status_code, MethodApi.DELETE)
            return JSONResponse(status_code=status_code, content={})
        else:
            raise NotExistingException

    except NotExistingException as de:
        # Log the error for debugging purposes
        log.error(f"Config {config_id} is disabled or not existing", exc_info=DEBUG)
        # Raise an HTTPException with status code 400 (Bad Request)
        return _error_response(
            status.HTTP_404_NOT_FOUND,
            "Config not found",
            "Config not found",
            MethodApi.DELETE,
        )

    except ValueError as ve:
        # Log the error for debugging purposes
        log.error(f"Validation error: {ve}", exc_info=DEBUG)
        return _error_response(
            status.HTTP_400_BAD_REQUEST,
            "Validation data error",
            "Failed to validate data. Please check the data format.",
            MethodApi.DELETE,
        )

    except Exception as e:
        # Handle any errors here
        log.error("Error handling request:", str(e), exc_info=DEBUG)
        # Raise an HTTPException with status code 500 (Internal Server Error)
        return _error_response(
            status.HTTP_500_INTERNAL_SERVER_ERROR,
            "Error handling request",
            "An error occurred while processing the request",
            MethodApi.DELETE,
        )


def _error_response(
    status_code: status, title: str, detail: str, method: MethodApi = MethodApi.TRACE
) -> JSONResponse:
    title_path = title.replace(" ", "-").lower()
    counter_metrics.increment_api_requests_total(status_code, method)
    return JSONResponse(
        content=ErrorMessage(
            type=f"/errors/{title_path}",
            title=title,
            status=status_code,
            detail=str(detail),
        ).__dict__,
        status_code=status_code,
    )


def _custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    title = "Payments Proxy WGolEpay"
    description = "Descrizione dell'API"
    terms_of_service = "https://example.com/terms"
    contact = {
        "name": "Opencity Labs Srl",
        "url": "https://opencitylabs.it/collabora-con-noi/",
        "email": "info@opencitylabs.it",
    }
    openapi_tags = [
        {"name": "Metrics"},
        {"name": "Status"},
        {"name": "Schema"},
        {"name": "Tenants"},
        {"name": "Configs"},
    ]
    servers = [
        {
            "url": EXTERNAL_API_URL,
            "description": APP_NAME,
            "x-sandbox": True,
        }
    ]
    openapi_schema = get_openapi(
        title=title,
        version=VERSION,
        tags=openapi_tags,
        servers=servers,
        description=description,
        terms_of_service=terms_of_service,
        contact=contact,
        routes=app.routes,
    )
    openapi_schema["info"]["x-api-id"] = APP_NAME
    openapi_schema["info"][
        "x-summary"
    ] = "Questo servizio si occupa di fare da intermediario tra Opencity Italia - Area Personle e il sistema di payments WGolEpay"
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = _custom_openapi

wgol_e_pay = WGolEPay(
    counter_metrics=counter_metrics,
    app_online_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_online_payment_url', payment_id='{id}')}",
    app_landing_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_landing_url', payment_id='{id}')}",
    app_notice_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_notice_file', payment_id='{id}')}",
    app_receipt_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_receipt_file', payment_id='{id}')}",
    app_update_url=f"{INTERNAL_API_URL}{app.url_path_for('check_for_updates', payment_id='{id}')}",
)


consumer_thread = Thread(
    target=kafka_consumer,
    args={counter_metrics: counter_metrics, wgol_e_pay: wgol_e_pay},
)
consumer_thread.start()

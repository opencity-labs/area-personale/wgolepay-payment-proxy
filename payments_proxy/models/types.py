from enum import Enum


class MethodApi(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATCH = "PATCH"
    TRACE = "TRACE"


class StorageType(Enum):
    AZURE = "azure"
    LOCAL = "local"
    S3 = "s3"


class RestHeaderClient(Enum):
    X_WWW_FORM_URLENCODED = {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
    }
    MULTIPART_FORM_DATA = {"Content-Type": "multipart/form-data"}
    APPLICATION_JSON = {"Content-Type": "application/json"}


class PaymentStatus(Enum):
    CREATION_PENDING = "CREATION_PENDING"
    CREATION_FAILED = "CREATION_FAILED"
    PAYMENT_PENDING = "PAYMENT_PENDING"
    PAYMENT_STARTED = "PAYMENT_STARTED"
    PAYMENT_CONFIRMED = "PAYMENT_CONFIRMED"
    PAYMENT_FAILED = "PAYMENT_FAILED"
    NOTIFICATION_PENDING = "NOTIFICATION_PENDING"
    COMPLETE = "COMPLETE"
    EXPIRED = "EXPIRED"


class SchemaType(Enum):
    CONFIG = "./configuration/config-form-schema.json"
    TENANT = "./configuration/tenant-form-schema.json"


class LinkType(Enum):
    INTERNAL_LINK = "internal"
    EXTERNAL_LINK = "external"


class WGolEPayPaymentStatus(Enum):
    APERTA = "Aperta"
    CHIUSA = "Chiusa"
    RENDICONTATA = "Rendicontata"


class WGolEPayTipoSoggetto(Enum):
    TYPE_HUMAN = "F"
    TYPE_LEGAL = "G"


class WGolEPayCodeSoggetto(Enum):
    FISCAL_CODE = "CF"
    VAT_CODE = "PI"


class StatusPayment(Enum):
    OK = "OK"
    KO = "KO"

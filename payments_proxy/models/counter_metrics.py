from prometheus_client import Counter, Histogram
from models.types import MethodApi
from settings import ENVIRONMENT, APP_NAME

proxy = APP_NAME.replace("-", "_")


class CounterMetrics:
    def __init__(self) -> None:
        self.failed_events_counter = Counter(
            "oc_payment_failed_events_total",
            "The total number of failed processed events",
            ["env", "proxy"],
        )
        self.success_events_counter = Counter(
            "oc_payment_success_events_total",
            "The total number of successfully processed events",
            ["env", "proxy"],
        )
        self.api_requests_counter = Counter(
            "oc_api_requests_total",
            "The total number of api call events",
            ["env", "status_code", "proxy", "method"],
        )
        self.validation_error_payments_counter = Counter(
            "oc_payment_validation_errors_total",
            "Number of processed payments with validation errors",
            ["env", "proxy"],
        )
        self.provider_error_payments_counter = Counter(
            "oc_payment_provider_errors_total",
            "Number of processed payments with provider errors",
            ["env", "proxy"],
        )
        self.internal_error_payments_counter = Counter(
            "oc_payment_internal_errors_total",
            "Number of processed payments with internal errors",
            ["env", "proxy"],
        )
        self.provider_latency_metrics = Histogram(
            "oc_payment_provider_latency",
            "Duration of provider requests",
            ["env", "proxy"],
            buckets=(0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5, 10.0, 15.0, 20.0, 30.0),
        )

    def increment_success(self):
        self.success_events_counter.labels(env=ENVIRONMENT, proxy=proxy).inc()

    def increment_failure(self):
        self.failed_events_counter.labels(env=ENVIRONMENT, proxy=proxy).inc()

    def increment_api_requests_total(
        self, status_code, method: MethodApi = MethodApi.TRACE
    ):
        self.api_requests_counter.labels(
            env=ENVIRONMENT, status_code=status_code, proxy=proxy, method=method.value
        ).inc()

    def inc_validation_error_payment_metrics(self):
        self.validation_error_payments_counter.labels(
            env=ENVIRONMENT, proxy=proxy
        ).inc()

    def inc_provider_error_payments_counter(self):
        self.provider_error_payments_counter.labels(env=ENVIRONMENT, proxy=proxy).inc()

    def inc_internal_error_payment_metrics(self):
        self.internal_error_payments_counter.labels(env=ENVIRONMENT, proxy=proxy).inc()

    def inc_provider_latency_metrics(self):
        return self.provider_latency_metrics.labels(env=ENVIRONMENT, proxy=proxy).time()

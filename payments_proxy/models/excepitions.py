class RetryException(Exception):
    pass


class JsonException(Exception):
    pass


class ExistException(Exception):
    pass


class NotExistingException(Exception):
    pass


class WGolEPayException(Exception):
    pass

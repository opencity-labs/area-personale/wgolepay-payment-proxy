from typing import List, Optional, Union
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from datetime import datetime


ERR_422_SCHEMA = {
    "description": "Validation Error",
    "content": {
        "application/problem+json": {
            "schema": {
                "$ref": "#/components/schemas/ErrorMessage",
            },
        },
    },
}


class Status(BaseModel):
    status: str


class ErrorMessage(BaseModel):
    type: str
    title: str
    status: Optional[int] = Field(..., format="int32")
    detail: Union[str, dict]
    instance: Optional[str]

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenantPut(BaseModel):
    tax_identification_number: str
    name: str
    username: str
    password: str
    cod_ente: str
    cod_conv: str
    payments_api_url: str
    active: bool

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenant(ConfigurationTenantPut):
    id: str


class ConfigurationTenantPatch(ConfigurationTenantPut):
    tax_identification_number: str = ""
    name: str = ""
    username: str = ""
    password: str = ""
    cod_ente: str = ""
    cod_conv: str = ""
    payments_api_url: str = ""
    active: bool = False


class RemoteCollectionModel(BaseModel):
    id: str = ""
    type: str = ""


class Receiver(BaseModel):
    tax_identification_number: str = ""
    name: str = ""
    iban: str = ""
    address: str = ""
    building_number: str = ""
    postal_code: str = ""
    town_name: str = ""
    country_subdivision: str = ""
    country: str = ""


class ConfigurationPut(BaseModel):
    active: bool
    notice_type: str
    payment_type: str
    cod_service: str
    amount: float
    reason: str
    category: str
    expire_at: str
    remote_collection: RemoteCollectionModel
    receiver: Receiver

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationPatch(ConfigurationPut):
    active: bool = True
    notice_type: str = ""
    payment_type: str = ""
    cod_service: str = ""
    amount: float = 0.0
    reason: str = ""
    category: str = ""
    expire_at: str = ""
    remote_collection: RemoteCollectionModel = RemoteCollectionModel()
    receiver: Receiver = Receiver()


class Configuration(ConfigurationPut):
    id: str
    tenant_id: str


class JSONResponseCustom(JSONResponse):
    media_type = "application/problem+json"

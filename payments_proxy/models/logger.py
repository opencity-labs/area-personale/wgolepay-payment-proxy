import logging
import sys
from settings import DEBUG


def get_logger():
    # Configurazione del logger
    file_handler = logging.FileHandler(filename="logfile.log")
    stdout_handler = logging.StreamHandler(stream=sys.stdout)
    handlers = [file_handler, stdout_handler]
    if DEBUG == "true":
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(
        level=level,
        format="%(asctime)s - %(levelname)s - %(message)s",
        handlers=handlers,
    )
    # Ottieni il logger
    log = logging.getLogger(__name__)

    return log

import os
from __init__ import __version__ as app_version

VERSION = app_version
APP_NAME = "payments-proxy-WGolEpay"
ENVIRONMENT = os.environ.get("ENVIRONMENT", "local")
DEBUG = os.environ.get("DEBUG", "true")
SENTRY_DSN = os.environ.get("SENTRY_DSN", "https://localhost:8080")
SENTRY_ENABLED = os.environ.get("SENTRY_ENABLED", "false").lower().strip() == "true"
EXTERNAL_API_URL = os.environ.get("EXTERNAL_API_URL", "http://localhost:8000")
INTERNAL_API_URL = os.environ.get("INTERNAL_API_URL", "http://localhost:8000")
GOTENBERG_URL = os.environ.get("GOTENBERG_URL", "http://gotenberg:3000/convert/html")
PAGOPA_CHECKOUT_URL = os.environ.get(
    "PAGOPA_CHECKOUT_URL", "https://api.uat.platform.pagopa.it/checkout/ec/v1/carts"
)
# Kafka
KAFKA_BOOTSTRAP_SERVERS = os.environ.get("KAFKA_BOOTSTRAP_SERVERS", "kafka:9092")
KAFKA_CONSUMER_GROUP = os.environ.get("KAFKA_CONSUMER_GROUP", "my_consumer_group")
KAFKA_CONSUMER_TOPIC = os.environ.get("KAFKA_CONSUMER_TOPIC", "payments")
KAFKA_PRODUCER_TOPIC = os.environ.get("KAFKA_PRODUCER_TOPIC", "payments")
##Storage
STORAGE_ENDPOINT = os.environ.get("STORAGE_ENDPOINT", "http://minio:9000")
STORAGE_TYPE = os.environ.get("STORAGE_TYPE", "local")
STORAGE_PATH_CONFIG = os.environ.get("STORAGE_PATH_CONFIG", "tenants/")
STORAGE_PATH_EVENTS = os.environ.get("STORAGE_PATH_EVENTS", "events/")
# azure
STORAGE_BASE_PATH = os.environ.get("STORAGE_BASE_PATH", "configuration/data/")
STORAGE_AZURE_ACCOUNT = os.environ.get("STORAGE_AZURE_ACCOUNT", "admin")
STORAGE_AZURE_KEY = os.environ.get(
    "STORAGE_AZURE_KEY",
    "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq==",
)
# s3
STORAGE_BUCKET = os.environ.get("STORAGE_BUCKET", "payments")
STORAGE_ACCESS_S3_KEY = os.environ.get("STORAGE_ACCESS_S3_KEY", "Test_key_innonation")
STORAGE_KEY_S3_ACCESS_SECRET = os.environ.get(
    "STORAGE_KEY_S3_ACCESS_SECRET", "innonation"
)
STORAGE_S3_REGION = os.environ.get("STORAGE_S3_REGION", "us-east-1")
STORAGE_LOCAL_PATH = os.environ.get("STORAGE_LOCAL_PATH", "configuration/data")
# test api
PREFIX_API = os.environ.get("PREFIX_API", "/")
PROXY_RETRY_URL = os.environ.get("PROXY_RETRY_URL", "http://locaholhost")
STAGE = os.environ.get("STAGE", "dev")

# CONST
EVENT_VERSION = "2.0"

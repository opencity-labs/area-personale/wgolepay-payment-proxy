import json
from azure.storage.blob import BlobServiceClient

from settings import (
    STORAGE_AZURE_ACCOUNT,
    STORAGE_AZURE_KEY,
    STORAGE_BUCKET,
    DEBUG,
)
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


class AzureStorage:
    def __init__(self, storage_endpoint: str):
        account_url = f"{storage_endpoint}/{STORAGE_AZURE_ACCOUNT}"
        self.blob_service_client = BlobServiceClient(
            account_url=account_url,
            credential={
                "account_name": STORAGE_AZURE_ACCOUNT,
                "account_key": STORAGE_AZURE_KEY,
            },
        )
        # Create the container if it doesn't exist
        self.container_client = self.blob_service_client.get_container_client(
            STORAGE_BUCKET
        )
        self._create_container_if_not_exists()

    def _create_container_if_not_exists(self):
        try:
            self.container_client.create_container()
            log.debug(f"Created container {STORAGE_BUCKET}")
        except Exception as e:
            if "ContainerAlreadyExists" in str(e):
                log.debug(f"Container {STORAGE_BUCKET} already exists")
            else:
                raise e

    def read(self, blob_name: str):
        blob_client = self.container_client.get_blob_client(blob_name)
        try:
            download_stream = blob_client.download_blob()
            data = download_stream.readall()
            log.debug(f"Blob {blob_name} read successfully.")
            return data.decode("utf-8")  # Return the data as string
        except Exception as e:
            log.error(f"Error reading blob {blob_name}: {str(e)}", exc_info=DEBUG)
            return None

    def save(self, blob_name: str, data: dict) -> bool:
        blob_client = self.container_client.get_blob_client(blob_name)
        try:
            blob_client.upload_blob(json.dumps(data), overwrite=True)
            log.debug(f"Blob {blob_name} uploaded successfully.")
            return True
        except Exception as e:
            log.error(f"Error uploading blob {blob_name}: {str(e)}", exc_info=DEBUG)
            raise e

    def delete(self, blob_name: str) -> str:
        blob_client = self.container_client.get_blob_client(blob_name)
        try:
            blob_client.delete_blob()
            log.debug(f"Blob {blob_name} deleted successfully.")
            return True
        except Exception as e:
            log.error(f"Error deleting blob {blob_name}: {str(e)}", exc_info=DEBUG)
            return False

    def get_all_files_in_path(self, path):
        """
        Ottieni la lista di tutti i file nel percorso specificato (prefix) all'interno del contenitore di Azure Blob Storage.
        Il percorso dovrebbe essere un prefisso che identifica una cartella o un percorso all'interno del contenitore.
        """

        # Assicurati che il percorso termini con '/' per identificare una directory
        if not path.endswith("/"):
            path += "/"

        files = []

        # Utilizza il prefisso (path) per filtrare i blob nel contenitore
        blob_list = self.container_client.list_blobs(name_starts_with=path)

        for blob in blob_list:
            # Ottieni il nome del blob
            blob_name = blob.name

            if blob_name.endswith("/"):
                # Il blob è un BlobPrefix (indicativo di una directory)
                # Rimuovi il prefisso dal nome del BlobPrefix per ottenere il nome della directory
                directory_name = blob_name.rstrip("/")
                relative_path = directory_name[
                    len(path) :
                ]  # Nome relativo della directory
                # Aggiungi il nome della directory alla lista dei file (come se fosse un file)
                files.append(
                    relative_path + "/"
                )  # Aggiungi '/' per indicare una directory

            else:
                # Il blob è un file effettivo
                # Rimuovi il prefisso dal nome del blob per ottenere il nome relativo del file
                relative_path = blob_name[len(path) :]
                # Aggiungi il nome relativo del file alla lista
                files.append(relative_path)

        return files

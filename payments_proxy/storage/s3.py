import json
from simple_aws_wrapper.config import AWSConfig
from simple_aws_wrapper.const.regions import Region
from simple_aws_wrapper.services.s3 import S3

from settings import (
    STORAGE_ACCESS_S3_KEY,
    STORAGE_BUCKET,
    STORAGE_KEY_S3_ACCESS_SECRET,
    STORAGE_ENDPOINT,
    STORAGE_S3_REGION,
)
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


class S3Storage:
    def __init__(self, storage_endpoint: str):
        AWSConfig().set_region(Region(STORAGE_S3_REGION))
        AWSConfig().set_endpoint_url(storage_endpoint)
        AWSConfig().set_aws_access_key_id(STORAGE_ACCESS_S3_KEY)
        AWSConfig().set_aws_secret_access_key(STORAGE_KEY_S3_ACCESS_SECRET)
        self.s3_client = S3()
        self.bucket = STORAGE_BUCKET

    def read(self, key: str) -> str:
        response = self.s3_client.get_str_file_content(self.bucket, key)
        log.debug(f"Reading file from s3: {response} with {key}")
        return response

    def save(self, key: str, data: dict):
        user_encode_data = json.dumps(data, indent=2).encode("utf-8")
        self.s3_client.put_object(user_encode_data, self.bucket, key)
        log.debug("Stored configuration")
        return True

    def get_all_files(self, base_path: str):
        return self.s3_client.list_object_keys(base_path)

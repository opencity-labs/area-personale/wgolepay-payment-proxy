import base64
import json
from models.logger import get_logger
from models.types import SchemaType
from storage.storage_manager import StorageManager
from settings import STORAGE_PATH_CONFIG, STORAGE_PATH_EVENTS, DEBUG

log = get_logger()
storage_manager = StorageManager()


def read_local_schema(schema_to_read: SchemaType):
    try:
        with open(schema_to_read.value, "r") as file:
            return file.read()
    except Exception as e:
        log.error(f"Error reading schema: {e}", exc_info=DEBUG)
        return None


def save_configuration(dicionary_configuration: dict):
    try:
        file_path = f"{dicionary_configuration['id']}.json"
        storage_manager.save(STORAGE_PATH_CONFIG + file_path, dicionary_configuration)
        log.debug(f"Configuration saved in: {STORAGE_PATH_CONFIG}{file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def check_configuration(key: str) -> dict:
    try:
        configuration = storage_manager.read(f"{STORAGE_PATH_CONFIG}{key}.json")
        log.debug(f"Configuration read from: {STORAGE_PATH_CONFIG}{key}.json")
        return json.loads(configuration)
    except Exception as e:
        log.warning(
            f"Missing configuration for config identifier {key}", exc_info=DEBUG
        )
        return None


def save_events(event_id: str, dicionary_events: dict):
    try:
        file_path = f"{event_id}.json"
        storage_manager.save(STORAGE_PATH_EVENTS + file_path, dicionary_events)
        log.debug(f"Events saved in: {STORAGE_PATH_EVENTS}{file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def check_events(key: str) -> dict:
    try:
        events_content = storage_manager.read(f"{STORAGE_PATH_EVENTS}{key}.json")
        log.debug(f"Events read from: {STORAGE_PATH_EVENTS}{key}.json")
        return json.loads(events_content)
    except Exception as e:
        log.warning(f"Missing events for config identifier {key}", exc_info=DEBUG)
        return None


def delete_configuration(key: str) -> bool:
    try:
        response = storage_manager.delete(f"{STORAGE_PATH_CONFIG}{key}.json")
        if response:
            log.debug(f"Configuration deleted from: {STORAGE_PATH_CONFIG}{key}.json")
        else:
            log.error(
                f"Configuration not deleted from: {STORAGE_PATH_CONFIG}{key}.json"
            )
        return response
    except Exception as e:
        log.error(f"Error deleting configuration: {e}", exc_info=DEBUG)
        return False


def disable_configuration(dicionary_configuration: dict):
    try:
        dicionary_configuration["active"] = False
        file_path = f"{dicionary_configuration['id']}.json"
        storage_manager.save(STORAGE_PATH_CONFIG + file_path, dicionary_configuration)
    except Exception as e:
        log.error(f"Error disabling configuration: {e}", exc_info=DEBUG)
        return False


def get_all_file_configuration():
    file_name_list = storage_manager.get_all_files(STORAGE_PATH_CONFIG)
    file_content_list = []
    for file in file_name_list:
        file_content_list.append(check_configuration(file))
    return file_content_list

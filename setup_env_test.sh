#!/bin/bash

# Aggiorna la lista dei pacchetti
apt-get update
apt-get install -qy netcat-openbsd wget unzip curl

# Installa rpk (Redpanda)
wget --quiet https://github.com/redpanda-data/redpanda/releases/latest/download/rpk-linux-amd64.zip
mkdir -p ~/.local/bin
export PATH="~/.local/bin:$PATH"
unzip rpk-linux-amd64.zip -d ~/.local/bin/
rpk topic create $KAFKA_TOPIC_NAME --brokers=$KAFKA_BOOTSTRAP_SERVERS

# Attendi che Kafka sia pronto
while ! nc -z kafka 9092; do
  echo "Waiting for Kafka..."
  sleep 1
done
echo "Kafka is ready"
  

# Configura e attiva l'ambiente virtuale Python
python -m venv venv
source venv/bin/activate

mkdir -p /code/payments_proxy/configuration/data
mkdir -p /code/payments_proxy/configuration/data/tenants
mkdir -p /code/payments_proxy/configuration/data/event

# Aggiorna pip e installa i pacchetti necessari
pip install --quiet --upgrade pip
pip install --quiet --no-cache-dir -r payments_proxy/requirements.txt

# Avvia il server FastAPI
cd payments_proxy
uvicorn main_api:app --host 0.0.0.0 --port 8000 --log-level debug &

# Attendi che il server FastAPI sia pronto
echo "Waiting for FastAPI server to be ready..."
while ! nc -z localhost 8000; do
  echo "Waiting for FastAPI..."
  sleep 1
done
echo "FastAPI is ready"

# Payments Proxy WGolEpay

## Cosa fa

Questo servizio legge dal topic payments, e per ogni evento creerà una posizione debitoria.
Inoltre, il servizio espone api per la gestione degli stati di pagamento e inoltre le configurazione per i tenant, i servizi, schema utilizzati dalla stanza per abilitare tale proxy di pagamento.

## Struttura del repository

### Configuration

Questo modulo serve a gestire le chiamate api di configurazione del servizio di pagamento.
Per testarlo bisogna andare `http://localhost:8060/docs` che punta al container di docker di fast-api.

### Kafka wrapper

Questo modulo serve a testare che si riesca a produrre e consumare un messaggio su kafka

### SOAP

Questo modulo serve a testare le chiamate API SOAP che verrano impiegate dal protcollo trwamite wsdl

### Storage

Questo modulo contiene lo [StoreManager] che gestisce le chiamate di read and save file in abse alla configurazione nel `settings.py`
Lo storage va configurato nel file `.env` settando la variabile [STORAGE_TYPE] che può essere local, s3 o azure

### Process

Questo modulo contiene la gestione del processamento dell informazioni consumate da kafka, controllando le configurazioni, ed effettua le chiamate al sistema per protcollare il messaggio.

## Come si usa

1. Per installare il progetto è sufficiente creare un nuovo ambiente virtuale Python con il comando

```bash
python -m virtualenv venv
```

2. attivato l'ambiente virtuale, procedere con l'installazione delle dipendenze con il comando

```bash
pip install --upgrade -r .\requirements.txt
```

3. Assicuriamoci di avere installato sulla nostra macchina [Docker Desktop](https://www.docker.com/products/docker-desktop/), dopo avviamo un terminale e ci posizioniamo nella root del progetto, qui avvieremo il comando che creerà i volumi necessari al funzionamento del progetto

```bash
`docker-compose up -d`,
```

4. Assicuratevi che venga creato il topic 'documents', e poi mettere il nome del topic nel file `.env` e `settings.py`

5. Assicuratevi che venga creato il [BUCKET] che sia settata la [REGION] L'[ACCESS-KEY] e [SECRET-KEY]

6. Per lanciare il comando pytest eseguire prima `cd protocol_proxy` successivamente lanciare il comando `$env:PYTHONPATH='.'; pytest --verbose --capture=tee-sys --disable-warnings --maxfail=1`

## Configurazione

Il sistema di configurazione prevede l'utilizzo del file `settings.py`
All'interno della directory principale esiste il file di esempio `.env.example` da poter usare
come traccia per la configurazione dell'ambiente.

### Configurazione variabili d'ambiente

| Nome                         | Default                                                          | Descrizione                                                              |
| ---------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------------------ | --- |
| ENVIRONMENT                  | local                                                            | Indica l'ambiente di sviluppo (locale, dev, prod, ecc.)                  |
| DEBUG                        | true                                                             | se impostato a true specifica il livello di log che viene stampato       |
| KAFKA_CONSUMER_TOPIC         | payments                                                         | Topic di kafka da cui si leggono gli eventi legati ai pagamenti          |
| KAFKA_PRODUCER_TOPIC         | payments                                                         | Topic di kafka in cui si inviano gli eventi legati ai pagamenti          |
| KAFKA_BOOTSTRAP_SERVERS      | kafka:9092                                                       | Endpoint di kafka                                                        |
| KAFKA_CONSUMER_GROUP         | payments                                                         | Nome del consumer group                                                  |     |
| PAGOPA_CHECKOUT_URL          | https://api.uat.platform.pagopa.it/checkout/ec/v1/carts          | Url per andare al pagamento PAGOPA                                       |
| EXTERNAL_API_URL             | http://localhost:8000                                            | Url del servizio implementato esterno                                    |
| INTERNAL_API_URL             | http://localhost:8000                                            | Url del servizio implementato interno                                    |
| GOTENBERG_URL                | http://gotenberg:3000/convert/html                               | Url per generare il pdf della ricevuta di pagamento                      |
| STORAGE_TYPE                 | local                                                            | type of storage (local/s3/azure)                                         |
| STORAGE_ACCESS_S3_KEY        | Test_key_innonation                                              | key di accesso a s3 storage                                              |
| STORAGE_KEY_S3_ACCESS_SECRET | innonation                                                       | utente di accesso a s3 storage storage                                   |
| STORAGE_S3_REGION            | us-east-1                                                        | location del cloud storage                                               |
| STORAGE_LOCAL_PATH           | configuration/data                                               | location della cartella locale storage                                   |
| STORAGE_BUCKET               | pagamento                                                        | Bucket di storage                                                        |
| STORAGE_BASE_PATH            | data/                                                            | base path dello storage                                                  |
| STORAGE_PATH_CONFIG          | tenant/                                                          | path cartella dove vengono salvate le configurazioni dentro il base path |
| STORAGE_PATH_EVENTS          | payments/                                                        | path cartella dove vengono salvate le configurazioni dentro il base path |
| STORAGE_AZURE_KEY            | Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq== | key di accesso ad azure                                                  |
| STORAGE_AZURE_ACCOUNT        | admin                                                            | utente di accesso ad azure                                               |
| STORAGE_ENDPOINT             | http://azurite:10000                                             | Endpoint di azure                                                        |
| PREFIX_API                   | /                                                                | prefisso per le Url delle API                                            |
| PROXY_RETRY_URL              | http://localhost                                                 | url di ritorno del nel proxy per i pagamenti                             |
| SENTRY_ENABLED               | false                                                            | se impostato a true viene abilitato sentry                               |
| SENTRY_DSN                   | https://<key>:<secret>@sentry.io/<project>                       | Endpoint per il monitoraggio di Sentry                                   |

### Configurazione Tenant

```json
{
  "tax_identification_number": "00000000000",
  "name": "Comune di test",
  "auth_type": "basic_auth",
  "username": "ACCOUNT",
  "password": "PASSWORD",
  "cod_ente": "0000000",
  "cod_conv": "0000000",
  "payments_api_url": " https://pagopa.arcaservizi.net:444/wgolepay-paservice/api",
  "active": true,
  "id": "9748e8f6-48b6-46f9-b89e-28ab1bc05727"
}
```

### Configurazione Servizio

```json
{
  "amount": 0.0,
  "category": "9/0105102TS/",
  "cod_service": "xxx",
  "expire_at": "xxx",
  "payment_type": "PAGOPA",
  "notice_type": "FATTURA",
  "reason": "xxx",
  "receiver": {
    "address": "xxx",
    "country": "xxx",
    "country_subdivision": "xxx",
    "iban": "xxx",
    "name": "xxx",
    "tax_identification_number": "xxx",
    "town_name": "xxx"
  },
  "remote_collection": {
    "id": "xxx",
    "type": "xxx"
  },
  "id": "xxx",
  "tenant_id": "xxx",
  "active": true
}
```

## Swagger

le API sono documente e consultabili all'indirizzo:
base url + /docs

## Stadio di sviluppo

il software si trova in fase

## Autori e Riconoscimenti

## License

## Contatti
